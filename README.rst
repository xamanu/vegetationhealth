================
vegetationhealth
================

Description
-----------

Obtains suitable indices for vegetation health from satellite imagery for a given region or area of interest.

Currently the following index is included:

* Normalized difference vegetation index (NDVI): With statistical values for the mean, standard deviation, and variance of all values from a defined area.

In the future more potential features may be added:

* Handle areas of interest that expand over imagery boundaries.
* Full data dump output.
* Machine readable output.
* Add additional indices and more advanced calculations for inspecting health of vegetation.

Requirements
------------

This is based on Python (>= 3.6) with Asynchronous I/O.

* ``aiohttp``
* ``geopandas``
* ``rasterio`` (and ``boto3``)
* ``shapely``

Installation
------------

| $ ``git clone https://gitlab.com/xamanu/vegetationhealth.git``
| $ ``cd vegetationhealth`` (you may also want to setup a virtual environment here)
| $ ``pip install -e .``

Usage
-----

$ ``vegetationhealth --area tests/assets/example.geojson``

Contributing
------------

For unit tests this software relies on `pytest`. For general liniting `pylint` and for consistent code style `black` and `flake8` are being
used. All linting instructions are defined in `.pre-commit` and can be used comforably with
`pre-commit <https://pre-commit.com/>`_.

**Install dependencies**

Testing and linting dependencies can be installed simply with:

| $ ``make install``
|
or alternatively you install them one by one:

| $ ``pip install .[tests]``
| $ ``pip install .[linters]``
| $ ``pre-commit install``
|
**Code linting**

| $ ``make check``
|
or

| $ ``pre-commit run --all-files``
|
**Testing**

Unit tests are run with pytest:

| $ ``pytest -s tests``

Code structure
--------------

* ``vegetationhealth.py`` - Main entrypoint for the programm. Deals with command line arguments, general programm routing and asynchronous calls.
* ``config.py`` - gathers and prepares all information to form a request.
* ``interfaces/`` - contains interfaces to STAC API and the handling of cloud-optimized-geotiff (cog) files.
* ``processors/`` - includes processors to computes information based on provided imagery. For now the Normalized difference vegetation index (NDVI) is implemented.

Copyright and copyleft
----------------------

Copyright (C) 2021

* Felix Delattre

Permission is hereby granted, free of charge, to any person obtaining a copy of this software
and associated documentation files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

See the `LICENSE <./LICENSE>`__ for the full license text.
