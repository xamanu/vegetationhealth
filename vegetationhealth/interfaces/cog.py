#!/usr/bin/env python3

# Copyright (C) 2021
#
# * Felix Delattre
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software
# and associated documentation files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or
# substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import logging
import rasterio

from geopandas import GeoSeries
from numpy.ma.core import MaskedArray
from rasterio import mask

from vegetationhealth import Configuration

logger = logging.getLogger(__name__)


class CogInterface:
    """Interface to cloud-optimized-geotiff (cog)."""

    @staticmethod
    async def query(config: Configuration, images_urls: list) -> list:
        """Gets cloud-optimied-geotiff efficiently, based on a provided area of interest used
        as a mask.

        Args:
            config (Configuration): An configuration object containing all defined options
                including the area of interest.

        Returns:
            list: List of imagery data per band stored in numpy masked arrays.
        """
        logger.info("Downloading cloud-optimized geotiffs for area of interest:")

        logger.info(config.area["geometry"])

        imagery = []
        for image_urls in images_urls:

            cogs = {}
            for band in image_urls:
                logger.info(f" - {band} band from {image_urls[band]}")
                cogs[band] = CogInterface._download_masked_data(
                    image_urls[band], config.area["geometry"]
                )
            imagery.append(cogs)

        return imagery

    @staticmethod
    def _download_masked_data(url: str, area: GeoSeries) -> MaskedArray:
        """
        Obtains cloud-optimized-geotiff data within an area of interest.

        Args:
            url (str): URL of cloud-optimized-geotiff file.
            area (GeoSeries): Area of interest.

        Returns:
            MaskedArray: Containig data of data obtained from cloud-optimized-geotiff.
        """
        with rasterio.open(url) as cog:

            reprojected_area = area.to_crs(cog.profile["crs"])
            masked_data, _ = mask.mask(cog, reprojected_area, crop=True, filled=False)
            return masked_data[0]
