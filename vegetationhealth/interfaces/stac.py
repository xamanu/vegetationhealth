#!/usr/bin/env python3

# Copyright (C) 2021
#
# * Felix Delattre
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software
# and associated documentation files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or
# substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import aiohttp
import logging
import shapely
import pprint

from vegetationhealth import Configuration

logger = logging.getLogger(__name__)


class StacInterface:
    """Interface to a standard STAC API.

    Searches for available imagery through the an STAC catalog based on search criteria.
    """

    @staticmethod
    async def query(config: Configuration) -> list:
        """Sends a request to a STAC API search endpoint, in order to receive information about
        available satellite imagery for a given area of interest. It returns a list of urls for
        imagery to be downloaded from, one link for each band, that has been requested.

        Args:
            config (Configuration): An configuration object containing all defined options.


        Returns:
            list: A list of cloud-optimized-geotiff imagery with urls for each band.
        """
        async with aiohttp.ClientSession() as session:

            api_endpoint = config.stac_api_endpoint + "/search"

            request = {
                "collections": [config.stac_api_collection],
                "intersects": shapely.geometry.mapping(config.area.unary_union),
                "limit": 1,
                "query": config.stac_query,
            }
            # TODO: To be extended for areas of interest that expand over several imagery.

            cogs_urls = []

            async with session.post(api_endpoint, json=request) as response:

                logger.info(
                    f"Requested STAC API: {api_endpoint} - with status code {response.status}"
                )

                assert (
                    response.status == 200
                ), f"Requested STAC API failed with status code {response.status}"

                response = await response.json()

                for feature in response["features"]:

                    logging.info("Selected imagery: " + pprint.pformat(feature["properties"]))

                    bands_urls = {}

                    # Collect all bands urls for a given source imagery
                    for value in feature["assets"].values():
                        feature_bands = value.get("eo:bands")

                        if feature_bands and len(feature_bands) == 1:
                            band_name = feature_bands[0].get("common_name")

                            # Only line up bands that have been specified in configuration
                            if band_name in config.bands:
                                bands_urls[band_name] = value.get("href")

                    cogs_urls.append(bands_urls)

            return cogs_urls
