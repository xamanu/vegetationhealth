#!/usr/bin/env python3

# Copyright (C) 2021
#
# * Felix Delattre
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software
# and associated documentation files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or
# substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import asyncio
import click
import logging
import datetime
import sys

from vegetationhealth import Configuration, CogInterface, StacInterface, NdviProcessor

# Add a logger printing error, warning, and info messages to the screen
logger = logging.getLogger()
logger.setLevel(logging.INFO)
logger.addHandler(logging.StreamHandler(sys.stdout))


@click.command()
@click.option(
    "--area",
    "-a",
    help="Path to a GeoJSON file containing a polygon of the area of interest.",
    required=True,
)
@click.option(
    "--stac-api-endpoint",
    default="https://earth-search.aws.element84.com/v0",
    help="URL for STAC API endpoint.",
)
@click.option(
    "--stac-api-collection",
    default="sentinel-s2-l2a-cogs",
    help="Name of STAC API collection. It is expected to provide cloud-optimized-geotiffs",
)
def main(area: str, stac_api_endpoint: str, stac_api_collection: str):
    """Vegetation health information tool.

    Obtains suitable indices for inspecting vegetation health from public satellite imagery for
    a given region.
    """
    logger.debug("'vegetationhealth' started at " + str(datetime.datetime.now()))

    # collect all configuration options and default values
    config = Configuration(
        dict(
            area_file=area,
            stac_api_endpoint=stac_api_endpoint,
            stac_api_collection=stac_api_collection,
        )
    )

    # initiate the asynchronous haviour with the main loop
    loop = asyncio.get_event_loop()
    loop.run_until_complete(vegetationhealth(config))

    # leave the program
    logger.debug("'vegetationhealth' finished at " + str(datetime.datetime.now()))
    sys.exit()


async def vegetationhealth(config: Configuration):
    """Main asynchronous entry point for the vegetationhealth program.

    Args:
        config (Configuration): An configuration object containing all defined options.
    """
    # obtain information about suitable imagery
    images_urls = await StacInterface.query(config)

    # download explored cloud-optimized-geotiff imagery
    imagery = await CogInterface.query(config, images_urls)

    # run calculation on downloaded imagery
    ndvi = await NdviProcessor.get_ndvi(imagery)

    # output to the user
    print("NDVI values for area inside '" + config.area_file + "':")
    print(" - mean: " + str(ndvi.mean()))
    print(" - variance: " + str(ndvi.var()))
    print(" - standard deviation: " + str(ndvi.std()))
