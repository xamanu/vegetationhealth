#!/usr/bin/env python3

# Copyright (C) 2021
#
# * Felix Delattre
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software
# and associated documentation files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or
# substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import logging
import geopandas

logger = logging.getLogger(__name__)


class Configuration:
    """Simple configuration class.

    Holds all necessary configuration options at one place.
    """

    def __init__(self, args: dict):
        """Init function.

        Collects configuration obtions and provides a single configuration object.

        Args:
            args (dict): Configuration options obtained from command line arguments.
        """
        # obtain the area of interest from input file
        self.area_file = args["area_file"]
        self.area = geopandas.read_file(self.area_file).to_crs(epsg=4326)
        logger.info(f"Loaded file with area of interest: '{self.area_file}'")

        # define the STAC API configuration
        self.stac_api_collection = args["stac_api_collection"]
        self.stac_api_endpoint = args["stac_api_endpoint"]
        self.stac_query = {
            "eo:cloud_cover": {"eq": 0},
            "sentinel:valid_cloud_cover": {"eq": True},
            "sort": {"field": "datetime", "direction": "desc"},
        }

        # define required bands (common band names as per)
        self.bands = ["red", "nir"]

        # define main coordinate reference system for calculations
        self.crs = 4326
