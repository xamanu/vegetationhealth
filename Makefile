check:
	pre-commit run --all-files

install:
	pip install .[tests]
	pip install .[linters]
	pip install -e .
	pre-commit install

format:
	pre-commit run --all-files black
