#!/usr/bin/env python3

# Copyright (C) 2021
#
# * Felix Delattre
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software
# and associated documentation files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or
# substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

from setuptools import setup, find_packages

tests_require = [
    "aioresponses",
    "pytest",
    "pytest-asyncio",
]

linters_require = ["pre-commit", "pylint"]

setup(
    name="vegetationhealth",
    version="0.0.1",
    description="Shows vegetation health for a given region.",
    long_description="""Obtains suitable indices for vegetation health from satellite imagery
    for a given region.""",
    url="https://gitlab.com/xamanu/vegetationhealth",
    license="MIT",
    keywords="API",
    install_requires=["aiohttp", "boto3", "click", "geopandas", "rasterio", "shapely"],
    author="Felix Delattre",
    tests_require=tests_require,
    extras_require={
        "tests": tests_require,
        "linters": linters_require,
    },
    entry_points={
        "console_scripts": ["vegetationhealth = vegetationhealth.vegetationhealth:main"]
    },
    packages=find_packages(),
    include_package_data=True,
    python_requires=">=3.6",
)
