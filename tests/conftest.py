#!/usr/bin/env python3

# Copyright (C) 2021
#
# * Felix Delattre
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software
# and associated documentation files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or
# substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

# flake8: noqa E501

import logging
import pytest

from numpy.ma.core import MaskedArray

from vegetationhealth import Configuration

logger = logging.getLogger(__name__)


@pytest.fixture
def config():
    """A test configuration object."""

    return Configuration(
        {
            "area_file": "tests/assets/example.geojson",
            "stac_api_endpoint": "https://earth-search.aws.element84.com/v0",
            "stac_api_collection": "sentinel-s2-l2a-cogs",
        }
    )


@pytest.fixture
def mocked_imagery_data():
    return [
        {
            "red": MaskedArray(
                data=[[0, None, 2], [3, 4, None]],
                mask=[[False, True, False], [False, False, True]],
                fill_value=0,
            ),
            "nir": MaskedArray(
                data=[[1, None, 0], [2, 4, None]],
                mask=[[False, True, False], [False, False, True]],
                fill_value=0,
            ),
        }
    ]


@pytest.fixture
def images_urls():
    return [
        {
            "band": "tests/assets/example.tif",
        }
    ]


@pytest.fixture
def stac_response():
    return {
        "type": "FeatureCollection",
        "stac_version": "1.0.0-beta.2",
        "stac_extensions": [],
        "context": {"page": 1, "limit": 1, "matched": 71, "returned": 1},
        "numberMatched": 71,
        "numberReturned": 1,
        "features": [
            {
                "type": "Feature",
                "stac_version": "1.0.0-beta.2",
                "stac_extensions": ["eo", "view", "proj"],
                "id": "S2A_32UQD_20210903_0_L2A",
                "bbox": [
                    11.927835243788259,
                    52.17549424226928,
                    13.6341671437856,
                    53.21198351848647,
                ],
                "geometry": {
                    "type": "Polygon",
                    "coordinates": [
                        [
                            [13.531018505310676, 52.17549424226928],
                            [11.927835243788259, 52.2262340066898],
                            [11.994655250134956, 53.21198351848647],
                            [13.6341671437856, 53.159413588610356],
                            [13.531018505310676, 52.17549424226928],
                        ]
                    ],
                },
                "properties": {
                    "datetime": "2021-09-03T10:26:12Z",
                    "platform": "sentinel-2a",
                    "constellation": "sentinel-2",
                    "instruments": ["msi"],
                    "gsd": 10,
                    "view:off_nadir": 0,
                    "proj:epsg": 32632,
                    "sentinel:utm_zone": 32,
                    "sentinel:latitude_band": "U",
                    "sentinel:grid_square": "QD",
                    "sentinel:sequence": "0",
                    "sentinel:product_id": "S2A_MSIL2A_20210903T102021_N0301_R065_T32UQD_20210903T131922",
                    "sentinel:data_coverage": 100,
                    "eo:cloud_cover": 0,
                    "sentinel:valid_cloud_cover": True,
                    "created": "2021-09-03T16:52:44.781Z",
                    "updated": "2021-09-03T16:52:44.781Z",
                },
                "collection": "sentinel-s2-l2a-cogs",
                "assets": {
                    "thumbnail": {
                        "title": "Thumbnail",
                        "type": "image/png",
                        "roles": ["thumbnail"],
                        "href": "https://roda.sentinel-hub.com/sentinel-s2-l1c/tiles/32/U/QD/2021/9/3/0/preview.jpg",
                    },
                    "overview": {
                        "title": "True color image",
                        "type": "image/tiff; application=geotiff; profile=cloud-optimized",
                        "roles": ["overview"],
                        "gsd": 10,
                        "eo:bands": [
                            {
                                "name": "B04",
                                "common_name": "red",
                                "center_wavelength": 0.6645,
                                "full_width_half_max": 0.038,
                            },
                            {
                                "name": "B03",
                                "common_name": "green",
                                "center_wavelength": 0.56,
                                "full_width_half_max": 0.045,
                            },
                            {
                                "name": "B02",
                                "common_name": "blue",
                                "center_wavelength": 0.4966,
                                "full_width_half_max": 0.098,
                            },
                        ],
                        "href": "https://sentinel-cogs.s3.us-west-2.amazonaws.com/sentinel-s2-l2a-cogs/32/U/QD/2021/9/S2A_32UQD_20210903_0_L2A/L2A_PVI.tif",
                        "proj:shape": [343, 343],
                        "proj:transform": [320, 0, 699960, 0, -320, 5900040, 0, 0, 1],
                    },
                    "info": {
                        "title": "Original JSON metadata",
                        "type": "application/json",
                        "roles": ["metadata"],
                        "href": "https://roda.sentinel-hub.com/sentinel-s2-l2a/tiles/32/U/QD/2021/9/3/0/tileInfo.json",
                    },
                    "metadata": {
                        "title": "Original XML metadata",
                        "type": "application/xml",
                        "roles": ["metadata"],
                        "href": "https://roda.sentinel-hub.com/sentinel-s2-l2a/tiles/32/U/QD/2021/9/3/0/metadata.xml",
                    },
                    "visual": {
                        "title": "True color image",
                        "type": "image/tiff; application=geotiff; profile=cloud-optimized",
                        "roles": ["overview"],
                        "gsd": 10,
                        "eo:bands": [
                            {
                                "name": "B04",
                                "common_name": "red",
                                "center_wavelength": 0.6645,
                                "full_width_half_max": 0.038,
                            },
                            {
                                "name": "B03",
                                "common_name": "green",
                                "center_wavelength": 0.56,
                                "full_width_half_max": 0.045,
                            },
                            {
                                "name": "B02",
                                "common_name": "blue",
                                "center_wavelength": 0.4966,
                                "full_width_half_max": 0.098,
                            },
                        ],
                        "href": "https://sentinel-cogs.s3.us-west-2.amazonaws.com/sentinel-s2-l2a-cogs/32/U/QD/2021/9/S2A_32UQD_20210903_0_L2A/TCI.tif",
                        "proj:shape": [10980, 10980],
                        "proj:transform": [10, 0, 699960, 0, -10, 5900040, 0, 0, 1],
                    },
                    "B01": {
                        "title": "Band 1 (coastal)",
                        "type": "image/tiff; application=geotiff; profile=cloud-optimized",
                        "roles": ["data"],
                        "gsd": 60,
                        "eo:bands": [
                            {
                                "name": "B01",
                                "common_name": "coastal",
                                "center_wavelength": 0.4439,
                                "full_width_half_max": 0.027,
                            }
                        ],
                        "href": "https://sentinel-cogs.s3.us-west-2.amazonaws.com/sentinel-s2-l2a-cogs/32/U/QD/2021/9/S2A_32UQD_20210903_0_L2A/B01.tif",
                        "proj:shape": [1830, 1830],
                        "proj:transform": [60, 0, 699960, 0, -60, 5900040, 0, 0, 1],
                    },
                    "B02": {
                        "title": "Band 2 (blue)",
                        "type": "image/tiff; application=geotiff; profile=cloud-optimized",
                        "roles": ["data"],
                        "gsd": 10,
                        "eo:bands": [
                            {
                                "name": "B02",
                                "common_name": "blue",
                                "center_wavelength": 0.4966,
                                "full_width_half_max": 0.098,
                            }
                        ],
                        "href": "https://sentinel-cogs.s3.us-west-2.amazonaws.com/sentinel-s2-l2a-cogs/32/U/QD/2021/9/S2A_32UQD_20210903_0_L2A/B02.tif",
                        "proj:shape": [10980, 10980],
                        "proj:transform": [10, 0, 699960, 0, -10, 5900040, 0, 0, 1],
                    },
                    "B03": {
                        "title": "Band 3 (green)",
                        "type": "image/tiff; application=geotiff; profile=cloud-optimized",
                        "roles": ["data"],
                        "gsd": 10,
                        "eo:bands": [
                            {
                                "name": "B03",
                                "common_name": "green",
                                "center_wavelength": 0.56,
                                "full_width_half_max": 0.045,
                            }
                        ],
                        "href": "https://sentinel-cogs.s3.us-west-2.amazonaws.com/sentinel-s2-l2a-cogs/32/U/QD/2021/9/S2A_32UQD_20210903_0_L2A/B03.tif",
                        "proj:shape": [10980, 10980],
                        "proj:transform": [10, 0, 699960, 0, -10, 5900040, 0, 0, 1],
                    },
                    "B04": {
                        "title": "Band 4 (red)",
                        "type": "image/tiff; application=geotiff; profile=cloud-optimized",
                        "roles": ["data"],
                        "gsd": 10,
                        "eo:bands": [
                            {
                                "name": "B04",
                                "common_name": "red",
                                "center_wavelength": 0.6645,
                                "full_width_half_max": 0.038,
                            }
                        ],
                        "href": "https://sentinel-cogs.s3.us-west-2.amazonaws.com/sentinel-s2-l2a-cogs/32/U/QD/2021/9/S2A_32UQD_20210903_0_L2A/B04.tif",
                        "proj:shape": [10980, 10980],
                        "proj:transform": [10, 0, 699960, 0, -10, 5900040, 0, 0, 1],
                    },
                    "B05": {
                        "title": "Band 5",
                        "type": "image/tiff; application=geotiff; profile=cloud-optimized",
                        "roles": ["data"],
                        "gsd": 20,
                        "eo:bands": [
                            {
                                "name": "B05",
                                "center_wavelength": 0.7039,
                                "full_width_half_max": 0.019,
                            }
                        ],
                        "href": "https://sentinel-cogs.s3.us-west-2.amazonaws.com/sentinel-s2-l2a-cogs/32/U/QD/2021/9/S2A_32UQD_20210903_0_L2A/B05.tif",
                        "proj:shape": [5490, 5490],
                        "proj:transform": [20, 0, 699960, 0, -20, 5900040, 0, 0, 1],
                    },
                    "B06": {
                        "title": "Band 6",
                        "type": "image/tiff; application=geotiff; profile=cloud-optimized",
                        "roles": ["data"],
                        "gsd": 20,
                        "eo:bands": [
                            {
                                "name": "B06",
                                "center_wavelength": 0.7402,
                                "full_width_half_max": 0.018,
                            }
                        ],
                        "href": "https://sentinel-cogs.s3.us-west-2.amazonaws.com/sentinel-s2-l2a-cogs/32/U/QD/2021/9/S2A_32UQD_20210903_0_L2A/B06.tif",
                        "proj:shape": [5490, 5490],
                        "proj:transform": [20, 0, 699960, 0, -20, 5900040, 0, 0, 1],
                    },
                    "B07": {
                        "title": "Band 7",
                        "type": "image/tiff; application=geotiff; profile=cloud-optimized",
                        "roles": ["data"],
                        "gsd": 20,
                        "eo:bands": [
                            {
                                "name": "B07",
                                "center_wavelength": 0.7825,
                                "full_width_half_max": 0.028,
                            }
                        ],
                        "href": "https://sentinel-cogs.s3.us-west-2.amazonaws.com/sentinel-s2-l2a-cogs/32/U/QD/2021/9/S2A_32UQD_20210903_0_L2A/B07.tif",
                        "proj:shape": [5490, 5490],
                        "proj:transform": [20, 0, 699960, 0, -20, 5900040, 0, 0, 1],
                    },
                    "B08": {
                        "title": "Band 8 (nir)",
                        "type": "image/tiff; application=geotiff; profile=cloud-optimized",
                        "roles": ["data"],
                        "gsd": 10,
                        "eo:bands": [
                            {
                                "name": "B08",
                                "common_name": "nir",
                                "center_wavelength": 0.8351,
                                "full_width_half_max": 0.145,
                            }
                        ],
                        "href": "https://sentinel-cogs.s3.us-west-2.amazonaws.com/sentinel-s2-l2a-cogs/32/U/QD/2021/9/S2A_32UQD_20210903_0_L2A/B08.tif",
                        "proj:shape": [10980, 10980],
                        "proj:transform": [10, 0, 699960, 0, -10, 5900040, 0, 0, 1],
                    },
                    "B8A": {
                        "title": "Band 8A",
                        "type": "image/tiff; application=geotiff; profile=cloud-optimized",
                        "roles": ["data"],
                        "gsd": 20,
                        "eo:bands": [
                            {
                                "name": "B8A",
                                "center_wavelength": 0.8648,
                                "full_width_half_max": 0.033,
                            }
                        ],
                        "href": "https://sentinel-cogs.s3.us-west-2.amazonaws.com/sentinel-s2-l2a-cogs/32/U/QD/2021/9/S2A_32UQD_20210903_0_L2A/B8A.tif",
                        "proj:shape": [5490, 5490],
                        "proj:transform": [20, 0, 699960, 0, -20, 5900040, 0, 0, 1],
                    },
                    "B09": {
                        "title": "Band 9",
                        "type": "image/tiff; application=geotiff; profile=cloud-optimized",
                        "roles": ["data"],
                        "gsd": 60,
                        "eo:bands": [
                            {
                                "name": "B09",
                                "center_wavelength": 0.945,
                                "full_width_half_max": 0.026,
                            }
                        ],
                        "href": "https://sentinel-cogs.s3.us-west-2.amazonaws.com/sentinel-s2-l2a-cogs/32/U/QD/2021/9/S2A_32UQD_20210903_0_L2A/B09.tif",
                        "proj:shape": [1830, 1830],
                        "proj:transform": [60, 0, 699960, 0, -60, 5900040, 0, 0, 1],
                    },
                    "B11": {
                        "title": "Band 11 (swir16)",
                        "type": "image/tiff; application=geotiff; profile=cloud-optimized",
                        "roles": ["data"],
                        "gsd": 20,
                        "eo:bands": [
                            {
                                "name": "B11",
                                "common_name": "swir16",
                                "center_wavelength": 1.6137,
                                "full_width_half_max": 0.143,
                            }
                        ],
                        "href": "https://sentinel-cogs.s3.us-west-2.amazonaws.com/sentinel-s2-l2a-cogs/32/U/QD/2021/9/S2A_32UQD_20210903_0_L2A/B11.tif",
                        "proj:shape": [5490, 5490],
                        "proj:transform": [20, 0, 699960, 0, -20, 5900040, 0, 0, 1],
                    },
                    "B12": {
                        "title": "Band 12 (swir22)",
                        "type": "image/tiff; application=geotiff; profile=cloud-optimized",
                        "roles": ["data"],
                        "gsd": 20,
                        "eo:bands": [
                            {
                                "name": "B12",
                                "common_name": "swir22",
                                "center_wavelength": 2.22024,
                                "full_width_half_max": 0.242,
                            }
                        ],
                        "href": "https://sentinel-cogs.s3.us-west-2.amazonaws.com/sentinel-s2-l2a-cogs/32/U/QD/2021/9/S2A_32UQD_20210903_0_L2A/B12.tif",
                        "proj:shape": [5490, 5490],
                        "proj:transform": [20, 0, 699960, 0, -20, 5900040, 0, 0, 1],
                    },
                    "AOT": {
                        "title": "Aerosol Optical Thickness (AOT)",
                        "type": "image/tiff; application=geotiff; profile=cloud-optimized",
                        "roles": ["data"],
                        "href": "https://sentinel-cogs.s3.us-west-2.amazonaws.com/sentinel-s2-l2a-cogs/32/U/QD/2021/9/S2A_32UQD_20210903_0_L2A/AOT.tif",
                        "proj:shape": [1830, 1830],
                        "proj:transform": [60, 0, 699960, 0, -60, 5900040, 0, 0, 1],
                    },
                    "WVP": {
                        "title": "Water Vapour (WVP)",
                        "type": "image/tiff; application=geotiff; profile=cloud-optimized",
                        "roles": ["data"],
                        "href": "https://sentinel-cogs.s3.us-west-2.amazonaws.com/sentinel-s2-l2a-cogs/32/U/QD/2021/9/S2A_32UQD_20210903_0_L2A/WVP.tif",
                        "proj:shape": [10980, 10980],
                        "proj:transform": [10, 0, 699960, 0, -10, 5900040, 0, 0, 1],
                    },
                    "SCL": {
                        "title": "Scene Classification Map (SCL)",
                        "type": "image/tiff; application=geotiff; profile=cloud-optimized",
                        "roles": ["data"],
                        "href": "https://sentinel-cogs.s3.us-west-2.amazonaws.com/sentinel-s2-l2a-cogs/32/U/QD/2021/9/S2A_32UQD_20210903_0_L2A/SCL.tif",
                        "proj:shape": [5490, 5490],
                        "proj:transform": [20, 0, 699960, 0, -20, 5900040, 0, 0, 1],
                    },
                },
                "links": [
                    {
                        "rel": "self",
                        "href": "https://earth-search.aws.element84.com/v0/collections/sentinel-s2-l2a-cogs/items/S2A_32UQD_20210903_0_L2A",
                    },
                    {
                        "rel": "canonical",
                        "href": "https://sentinel-cogs.s3.us-west-2.amazonaws.com/sentinel-s2-l2a-cogs/32/U/QD/2021/9/S2A_32UQD_20210903_0_L2A/S2A_32UQD_20210903_0_L2A.json",
                        "type": "application/json",
                    },
                    {
                        "title": "sentinel-s2-l2a-aws/workflow-publish-sentinel/tiles-32-U-QD-2021-9-3-0",
                        "rel": "via-cirrus",
                        "href": "https://cirrus-earth-search.aws.element84.com/v0/catid/sentinel-s2-l2a-aws/workflow-publish-sentinel/tiles-32-U-QD-2021-9-3-0",
                    },
                    {
                        "title": "Source STAC Item",
                        "rel": "derived_from",
                        "href": "https://cirrus-v0-data-1qm7gekzjucbq.s3.us-west-2.amazonaws.com/sentinel-s2-l2a/32/U/QD/2021/9/S2A_32UQD_20210903_0_L2A/S2A_32UQD_20210903_0_L2A.json",
                        "type": "application/json",
                    },
                    {
                        "title": "sentinel-s2-l2a/workflow-cog-archive/S2A_32UQD_20210903_0_L2A",
                        "rel": "via-cirrus",
                        "href": "https://cirrus-earth-search.aws.element84.com/v0/catid/sentinel-s2-l2a/workflow-cog-archive/S2A_32UQD_20210903_0_L2A",
                    },
                    {
                        "rel": "parent",
                        "href": "https://earth-search.aws.element84.com/v0/collections/sentinel-s2-l2a-cogs",
                    },
                    {
                        "rel": "collection",
                        "href": "https://earth-search.aws.element84.com/v0/collections/sentinel-s2-l2a-cogs",
                    },
                    {
                        "rel": "root",
                        "href": "https://earth-search.aws.element84.com/v0/",
                    },
                ],
            }
        ],
        "links": [
            {
                "rel": "next",
                "title": "Next page of results",
                "method": "POST",
                "href": "https://earth-search.aws.element84.com/v0/search",
                "merge": False,
                "body": {
                    "intersects": {
                        "type": "Polygon",
                        "coordinates": [
                            [
                                [13.09621810913086, 52.468560246396606],
                                [13.10068130493164, 52.52154811647113],
                                [13.06617736816406, 52.53460237630518],
                                [13.01107406616211, 52.53272280202344],
                                [13.010902404785156, 52.52906784393992],
                                [13.001804351806639, 52.528127948407935],
                                [12.996997833251951, 52.5296944297867],
                                [12.98309326171875, 52.52353261403368],
                                [12.989788055419922, 52.5131913538099],
                                [12.969017028808594, 52.514236036066244],
                                [12.987041473388672, 52.49783165855702],
                                [13.05776596069336, 52.45946123411788],
                                [13.068923950195312, 52.45977502447022],
                                [13.09621810913086, 52.468560246396606],
                            ]
                        ],
                    },
                    "query": {
                        "eo:cloud_cover": {"eq": 0},
                        "sentinel:valid_cloud_cover": {"eq": True},
                        "sort": {"field": "datetime", "direction": "desc"},
                    },
                    "collections": ["sentinel-s2-l2a-cogs"],
                    "page": 2,
                    "limit": 1,
                },
            }
        ],
    }
