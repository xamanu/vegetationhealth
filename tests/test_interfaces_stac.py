#!/usr/bin/env python3

# Copyright (C) 2021
#
# * Felix Delattre
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software
# and associated documentation files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or
# substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import logging
import pytest

from aioresponses import aioresponses

from vegetationhealth import StacInterface

logger = logging.getLogger(__name__)


@pytest.mark.asyncio
async def test_stac_response(config, stac_response):
    with aioresponses() as mocked:

        mocked.post(config.stac_api_endpoint + "/search", status=200, payload=stac_response)

        config.bands = ["red", "nir"]

        response = await StacInterface.query(config)
        response_result = response.pop()

        assert response_result == {
            "red": "https://sentinel-cogs.s3.us-west-2.amazonaws.com/sentinel-s2-l2a-cogs/32/U/QD/2021/9/S2A_32UQD_20210903_0_L2A/B04.tif",  # noqa: E501
            "nir": "https://sentinel-cogs.s3.us-west-2.amazonaws.com/sentinel-s2-l2a-cogs/32/U/QD/2021/9/S2A_32UQD_20210903_0_L2A/B08.tif",  # noqa: E501
        }


@pytest.mark.asyncio
async def test_stac_404_error(config):
    with aioresponses() as mocked:

        status = 404
        mocked.post(config.stac_api_endpoint + "/search", status=status)

        with pytest.raises(Exception) as e:
            await StacInterface.query(config)

        assert str(e.value) == "Requested STAC API failed with status code 404"
